I'm a Computer Science student
Link to expo: https://snack.expo.dev/@alegottardi/dordle-react-native-new
To run locally, execute 'yarn' followed by 'yarn expo start'

Note that the expected screen size is the one of a normal computer, and on a smartphone the game may not fit completely
