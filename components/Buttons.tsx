import React from "react"
import {Pressable, Text, TextInput} from "react-native"
import {styles} from "../style"

const SingleButton = ({title, onPress}: {title: string; onPress: () => void}) => {
  return <BlackWhiteButton focused={true} text={title} onValueChanged={onPress} />
}

const BlackWhiteButton = ({
  focused,
  text,
  onValueChanged,
}: {
  focused: boolean
  text: string
  onValueChanged: (value: string) => void
}) => {
  return (
    <Pressable
      style={[styles.toggle, focused && styles.toggleFocused]}
      onPress={() => onValueChanged(text)}
    >
      <Text style={[styles.text, focused && styles.toggleFocused]}>{text}</Text>
    </Pressable>
  )
}

const BlackWhiteInput = ({
  focused,
  onFocus,
  onValueChanged,
}: {
  focused: boolean
  onFocus: () => void
  onValueChanged: (value: string) => void
}) => {
  const [number, onChangeNumber] = React.useState("")

  return (
    <TextInput
      style={[styles.text, styles.toggle, focused && styles.toggleFocused]}
      onChangeText={(value: string) => {
        onChangeNumber(value)
        !isNaN(Number(value)) && onValueChanged(value)
      }}
      onFocus={onFocus}
      value={number}
      keyboardType="numeric"
      placeholder="NUMBER"
    />
  )
}

export {SingleButton, BlackWhiteButton, BlackWhiteInput}
