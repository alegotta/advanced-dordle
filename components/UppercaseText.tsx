import {StyleProp, Text, TextStyle} from "react-native"
import {styles} from "../style"

const getStyleFromProp = (
  h1: boolean,
  h2: boolean,
  h3: boolean,
  h4: boolean,
): StyleProp<TextStyle>[] => {
  if (h1) return [styles.text, styles.title, styles.header]
  else if (h2) return [styles.text, styles.title]
  else if (h3) return [styles.text]
  /*if (h4)*/ else return [styles.lightText]
}

const UppercaseText = ({
  children,
  h1 = false,
  h2 = false,
  h3 = false,
  h4 = true,
}: {
  children?: React.ReactNode
  h1?: boolean
  h2?: boolean
  h3?: boolean
  h4?: boolean
}) => {
  return <Text style={getStyleFromProp(h1, h2, h3, h4)}>{children}</Text>
}

export {UppercaseText}
