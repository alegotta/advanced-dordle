import AsyncStorage from "@react-native-async-storage/async-storage"
import * as Device from "expo-device"
import * as Haptics from "expo-haptics"
import {allwords} from "./allwords"
import {answers} from "./answers"
import {
  bestGuesses,
  colors,
  CssColors,
  GameState,
  GameVariant,
  Guess,
  Letter,
  maxGuessesForVariant,
  newGame as logicNewGame,
  processGuess,
  rateGuesses,
  Statistic,
  Status,
  stringToWord,
  WordGuess,
} from "./logic"

type SavedStats = {
  stats: Statistic[]
}

const saveStatsistics = async (statistics: Statistic[]): Promise<void> => {
  await AsyncStorage.setItem("statistics", JSON.stringify({stats: statistics}))
}
const getStatistics = async (): Promise<Statistic[]> => {
  const stats = await AsyncStorage.getItem("statistics")
  if (stats !== null && stats != "{}") return (JSON.parse(stats) as SavedStats).stats
  else return []
}

const newGame = (prevGame: GameState): GameState => {
  const gameObj = logicNewGame(prevGame)
  void saveStatsistics(gameObj.statistics)
  return gameObj
}

const initGame = async (): Promise<GameState> => {
  return newGame({
    variant: "wordle",
    guesses: [],
    candidateGuess: "",
    words: answers,
    answers: ["random"],
    validwords: allwords,
    maxGuesses: 5,
    mode: "easy",
    statistics: await getStatistics(),
    status: "next",
  })
}

type UnicodeColor = "🟩" | "🟨" | "🟥" | "⬜"
type UnicodeWord = UnicodeColor[]
const kindToUnicodeColor = ([color]: CssColors[]): UnicodeColor => {
  if (color === colors.correct) return "🟩"
  else if (color === colors.misplaced) return "🟨"
  else if (color === colors.wrong) return "🟥"
  else return "⬜"
}
const unicodeGridFor = (gameState: GameState, guessIdx = 0): UnicodeWord[] => {
  return formatGuesses(gameState, guessIdx).map((guess) =>
    guess.map((letter) => kindToUnicodeColor(letter.colors)),
  )
}
const getGameSchema = (gameState: GameState): string => {
  const schema =
    gameState.variant === "dordle"
      ? [unicodeGridFor(gameState, 0), unicodeGridFor(gameState, 1)]
      : [unicodeGridFor(gameState, 0)]

  const schemaString = schemaToString(schema)
  return [
    `${gameState.variant}: ${gameState.guesses.length}/${gameState.maxGuesses}`,
    ...schemaString,
  ].join("\n")
}

const schemaToString = (schema: UnicodeWord[][]): string[] => {
  const getRow = (schema: UnicodeWord[][], idx: number): string =>
    schema.map((grid) => grid[idx].join("")).join("\t")

  return schema[0].map((_, idx) => getRow(schema, idx))
}

const vibrate = (status: Status): void => {
  let impact = Haptics.ImpactFeedbackStyle.Light
  if (status === "retry" || status === "invalid_hard") impact = Haptics.ImpactFeedbackStyle.Medium
  else if (status === "win" || status === "lost") impact = Haptics.ImpactFeedbackStyle.Heavy

  const notOnWeb = Device.brand !== null
  if (notOnWeb) void Haptics.impactAsync(impact)
}

// fill the guesses array so it always returns the same amount of rows
const fillArray = (words: WordGuessWithColors[], maxGuesses: number): WordGuessWithColors[] => [
  ...words,
  ...Array(maxGuesses - words.length).fill(stringToLetterGuess("")),
]

const colorStyle = (kind: Guess): CssColors => {
  if (kind === "absent") return colors.wrong as CssColors
  else if (kind === "misplaced") return colors.misplaced as CssColors
  else if (kind === "correct") return colors.correct as CssColors
  else return colors.untried as CssColors
}

const formatGuesses = (
  {guesses, maxGuesses, candidateGuess, answers}: GameState,
  guessIdx = 0,
): WordGuessWithColors[] => {
  // we fill a board with the existing guesses

  // for each guess,  we evaluate it (we don't store the guess status, even if we could)
  // then we format and print the guess
  let discovered = false // in dordle mode, don't print new guesses if the word has already been discovered
  const ratedGuesses: WordGuessWithColors[] = guesses
    .map((guess) => {
      if (discovered) return []
      const ratedGuess: WordGuess = rateGuesses(guess, answers[guessIdx])
      if (answers[guessIdx] === letterGuessToString(ratedGuess)) discovered = true

      return ratedGuess.map((g) => {
        return {letter: g.letter, colors: [colorStyle(g.kind), colorStyle(g.kind)]}
      })
    })
    .filter((guess) => guess.length === 5)

  if (candidateGuess.length > 0 && guesses.every((guess) => guess !== answers[guessIdx]))
    return fillArray([...ratedGuesses, stringToLetterGuess(candidateGuess)], maxGuesses)
  else return fillArray(ratedGuesses, maxGuesses)
}

const letterGuessToString = (word: WordGuess): string => {
  return word.map((elem) => elem.letter.character).join("")
}
const stringToLetterGuess = (word: string): WordGuessWithColors => {
  const guessWord: WordGuessWithColors = stringToWord(word).map((letter) => {
    return {letter: letter, colors: [colorStyle("untried"), colorStyle("untried")]}
  })
  return [
    ...guessWord,
    ...Array(5 - word.length).fill({
      letter: " ",
      colors: [colorStyle("untried"), colorStyle("untried")],
    }),
  ]
}

// a type to to accomodate multiple guess statuses, used for the keyboard
type LetterGuessWithColors = {
  letter: Letter
  colors: CssColors[]
}
type WordGuessWithColors = LetterGuessWithColors[]

const formatKeyboard = ({guesses, answers, variant}: GameState): WordGuessWithColors => {
  const keyGuesses: WordGuessWithColors[] = answers.map((answer) => {
    return bestGuesses("QWERTYUIOPASDFGHJKL ⌫ZXCVBNM ⏎", guesses, answer).map((g, idx) => {
      return {letter: {character: g.letter.character, index: idx}, colors: [colorStyle(g.kind)]}
    })
  })
  return keyGuesses[0].map((elem, i) => {
    let other: CssColors[] = []
    if (variant === "dordle")
      other = [...keyGuesses[1][i].colors] // in dordle mode, also append the second guess status
    else other = elem.colors //otherwise return the same color
    return {...elem, colors: [...elem.colors, ...other]}
  })
}

const appendLetterToPartialGuess = (gameState: GameState, letter: string): GameState => {
  if (gameState.candidateGuess.length < 5) {
    const newPartialGuess: string = gameState.candidateGuess.concat(letter)
    return {...gameState, candidateGuess: newPartialGuess}
  } else {
    return gameState
  }
}
const removeLetterToPartialGuess = (gameState: GameState): GameState => {
  if (gameState.candidateGuess.length > 0) {
    const newPartialGuess: string = gameState.candidateGuess.substring(
      0,
      gameState.candidateGuess.length - 1,
    )
    return {...gameState, candidateGuess: newPartialGuess}
  } else {
    return gameState
  }
}

const letterPress = (letter: string, gameState: GameState): GameState => {
  if (letter.trim().length === 0) return gameState
  else if (gameState.status === "win" || gameState.status === "lost") return newGame(gameState)
  else if (letter === "⏎") return processGuess(gameState)
  else if (letter === "⌫") return removeLetterToPartialGuess(gameState)
  else return appendLetterToPartialGuess(gameState, letter)
}

// return an informative status message
const getStatusMessage = (status: Status): string => {
  // we act on the status
  switch (status) {
    case "retry":
      // nothing changes, we ask for new input
      return "Invalid guess, please retry"
    case "invalid_hard":
      return "You didn't use all previously discovered letters"
    // we call the function again
    case "win":
      // if we win, we start a new game
      return "YOU WON! Press enter to start a new play"
    case "lost":
      // same if we lost
      return "You lost :( Press enter to start a new play"
    case "next":
      // otherwise, it's a regular turn of the game
      // we call the function again
      return "Your turn"
    case "new":
      return "Ready for a new game?"
  }
}

// a very basic statistics rendering
// "7" means that the game was lost
// an improved version would render this more clearly
const stats = (statistics: Statistic[], variant: GameVariant): string[] => {
  return statistics
    .filter((play) => play.variant === variant)
    .map((play) => {
      if (play.guessesNo > maxGuessesForVariant[variant])
        return `${play.answers.join(", ")}: not guessed`
      else return `${play.answers.join(", ")}: guessed in ${play.guessesNo} attempts`
    })
}

export {
  newGame,
  initGame,
  getGameSchema,
  vibrate,
  formatGuesses,
  formatKeyboard,
  letterPress,
  getStatusMessage,
  stats,
  LetterGuessWithColors,
  WordGuessWithColors,
}
