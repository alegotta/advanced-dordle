import {DictionaryResponse} from "./DictionaryResponse"

type ApiResponse = {
  data: string
  isLoading: boolean
  hasError: boolean
}

const getFromApi = async <R>(
  url: string,
  transformData: (data: R) => string,
): Promise<ApiResponse> => {
  let data = "Loading"
  let isLoading = true
  let hasError = false

  try {
    const result = await fetch(url)
    const parsed = (await result.json()) as R
    data = transformData(parsed)
  } catch (error) {
    console.warn(error)
    hasError = true
    data = "Error"
  } finally {
    isLoading = false
  }

  return {data, isLoading, hasError}
}

type DictionaryHint = "None" | "Definition" | "Example" | "Type"
const getDictionaryHint = async (word: string, action: DictionaryHint): Promise<ApiResponse> => {
  return await getFromApi(
    `https://api.dictionaryapi.dev/api/v2/entries/en/${word}`,
    dictionaryMapper(word, action),
  )
}
const dictionaryMapper = (
  word: string,
  action: DictionaryHint,
): ((data: DictionaryResponse[]) => string) => {
  let fun: (data: DictionaryResponse[]) => string
  switch (action) {
    case "Definition":
      fun = (data: DictionaryResponse[]) => `${data[0].meanings[0].definitions[0].definition}`
      break
    case "Type":
      fun = (data: DictionaryResponse[]) => `${data[0].meanings[0].partOfSpeech}`
      break
    case "Example":
      fun = (data: DictionaryResponse[]) => exampleMapper(data)
      break
    default:
      fun = (data: DictionaryResponse[]) => JSON.stringify(data)
  }
  return (data: DictionaryResponse[]): string => {
    return fun(data).replace(`/${word.toLowerCase()}/g`, "_".repeat(word.length))
  }
}
const exampleMapper = (data: DictionaryResponse[]): string => {
  const examples: string[] = data[0].meanings.map(
    (meaning) => meaning.definitions.find((def) => def.example !== undefined)?.example,
  )

  if (examples.length === 0) return "no examples found"
  else return examples[0].toLowerCase()
}

export {getFromApi, getDictionaryHint, DictionaryHint, ApiResponse}
