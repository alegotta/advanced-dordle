// type definitions

// status for a guess: correct (green), misplaced (yellow), absent (red)
// also including the letters that were not tried
// (for the keyboard)
type Guess = "correct" | "misplaced" | "absent" | "untried"

// a single letter, that keeps its index
interface Letter {
  character: string
  index: number
}

// a word is a sequence of letters
type Word = Letter[]

// the status of a guess for a letter
interface LetterGuess {
  letter: Letter
  kind: Guess
}
type WordGuess = LetterGuess[]

// a temporary structure to accumulate data
// it contains:
// the letters for which we know the status
// the remaining letters in the guess
// the remaining letters in the answer
interface GuessData {
  result: WordGuess
  guessLeft: Letter[]
  wordLeft: Letter[]
}

// a function to convert a string into a word
const stringToWord = (str: string): Word => {
  // convert a string into a list
  const [...charList] = str
  // we convert each character to a Letter, by keeping its index
  // by giving a 2 argument function to map
  return charList.map((c, idx) => ({character: c, index: idx}))
}

// the function that finds the "green" letters in the guess
// and "removes" them from the set of letters to consider further
// a recursive function
// if the first letter of the guess and the first letter of the answer are the same, then:
// - we add a "correct" guess to the result
// - we "remove" the letter from the guess (we don't add it to the guess data)
// - we "remove" the letter from the answer (we don't add it to the guess data)
// otherwise, the letter is not "green", so:
// - we do not add it as a result
// - we keep the letter in the guess (we add it to the guess data)
// - we keep the letter in the answer (we add it to the guess data)
const corrects = (guess: Word, answer: Word): GuessData => {
  // the base case of the recursion, we reached the end of the list
  // we create an empty GuessData object
  if (guess.length === 0) return {result: [], guessLeft: [], wordLeft: []}
  // otherwise, we split the guess and answer in the first letter and the rest of the lists
  const [currGuess, ...guesses] = guess
  const [currAnswer, ...answers] = answer
  // then we call the recursion on the rest of the lists,
  // and we get the GuessData object from the rest of the word
  const {result, guessLeft, wordLeft} = corrects(guesses, answers)
  // we determine if the first letter is "green" or not
  if (currGuess.character === currAnswer.character) {
    // it is green, we add it in the result
    // we create a "correct" letterguess
    const correct: LetterGuess = {letter: currGuess, kind: "correct"}
    // we add it to the result from the rest of the recursion
    // we don't add the letter to the letters left (we "remove" the letter)
    return {result: [correct, ...result], guessLeft, wordLeft}
  }
  // otherwise, the letter is not green
  // we don't change the result, but we keep the letters, by adding them to guessLeft and wordLeft
  return {
    result,
    guessLeft: [currGuess, ...guessLeft],
    wordLeft: [currAnswer, ...wordLeft],
  }
}

// two functions to find the "yellow" letters

// a function that goes through a list, and determines whether it contains a given letter
// if the letter is present, it is "removed" from the list
// if the letter is absent, the list is unchanged
// notice we use the javascript list destructuring to split the list two (first and rest)
// the alternative would be to do like in lines 76 and 77
// const [first, ...rest] = list
const hasLetter = (letter: Letter, [first, ...rest]: Letter[]): [boolean, Letter[]] => {
  // first is undefined, we reached the end of the list,
  // we did not find the letter
  // we return false
  if (first === undefined) return [false, []]
  // else, if the first letter is the one we look for:
  // we return true, and the rest of the list
  // we don't need to traverse it, since we found the letter once already
  // we "remove" the letter, by not adding it to the result
  if (letter.character === first.character) return [true, rest]
  // otherwise, we keep looking for the letter in the rest of the list
  const [bool, letters] = hasLetter(letter, rest)
  // we return the result of the search, and we add the letter to the result
  return [bool, [first, ...letters]]
}

// we use this previous function once for each letter of the guess
// this function works a bit like "corrects", but it only goes through one list at a time
// because it uses the "hasLetter" function to go through the second list
const misplaced = (guess: Word, answer: Word): GuessData => {
  // if we reach the end of the list, we create a new GuessData object
  // anything that is left in the answer at this point, are letters that were not matched
  // so we add them here
  if (guess.length === 0) return {result: [], guessLeft: [], wordLeft: answer}
  // we split the guess list in two
  const [currGuess, ...guesses] = guess
  // we check if the word has the letter or not
  const [letterFound, answerLeft] = hasLetter(currGuess, answer)
  // we call the recursion, with the remainder of the list
  // and with the result of hasLetter (which may be shorter than before, or not)
  const {result, guessLeft, wordLeft} = misplaced(guesses, answerLeft)
  if (letterFound) {
    //if we found the letter, we add a "yellow" guess to the result
    const misplacedLetter: LetterGuess = {
      letter: currGuess,
      kind: "misplaced",
    }
    // we "remove" it from the guesses left, by not adding it
    return {result: [misplacedLetter, ...result], guessLeft, wordLeft}
  }
  // otherwise, we do not add a guess
  // we add the letter to the letters that are still to guess
  return {result, guessLeft: [currGuess, ...guessLeft], wordLeft: wordLeft}
}

// this function brings all the pieces together, and finds the green, yellow, and red letters
// using the previous functions
const rateGuesses = (guess: string, answer: string): WordGuess => {
  // first we get the green letters
  const {result, guessLeft, wordLeft} = corrects(stringToWord(guess), stringToWord(answer))
  // then we get the yellow letters
  const secondResult = misplaced(guessLeft, wordLeft)
  // whatever is left at the end of the second step, is a "red" letter
  const incorrects: WordGuess = secondResult.guessLeft.map((letter) => ({
    letter,
    kind: "absent",
  }))

  // finally, we put all the lists together, green,yellow, and red
  // we recover the original order of the letters, by sorting them according to their index
  const allResults = [...result, ...secondResult.result, ...incorrects].sort(
    (a, b) => a.letter.index - b.letter.index,
  )
  return allResults
}

// a simple test function for evalauting the guesses
// the expected result is a string of "colors"
// const testGuess = (guess: string, answer: string, expected: string): void => {
//   // we call the rateGuesses function, and transform the output
//   // so that it fits the format of the "expected" argument
//   const result = rateGuesses(guess, answer)
//     .map((g: LetterGuess) => {
//       switch (g.kind) {
//         case "correct":
//           return "G"
//         case "misplaced":
//           return "Y"
//         case "absent":
//           return "R"
//       }
//     })
//     .join("")
//   console.assert(
//     result === expected,
//     "\nguess:  %s\nanswer: %s \nexpect: %s\nresult: %s",
//     guess,
//     answer,
//     expected,
//     result,
//   )
// }

// testGuess("aa", "aa", "GG")
// testGuess("aab", "aaa", "GGR")
// testGuess("hello", "wells", "RGGGR")
// testGuess("rotor", "robot", "GGYGR")
// testGuess("roots", "robot", "GGYYR")
// testGuess("robot", "roots", "GGRYY")
// testGuess("robot", "rotor", "GGRGY")
// testGuess("mummy", "money", "GRRRG")
// testGuess("money", "mummy", "GRRRG")
// testGuess("mummy", "hummm", "YGGGR")
// testGuess("hummm", "mummy", "RGGGY")
// testGuess("mummu", "muumy", "GGRGY")
// testGuess("abcde", "edcba", "YYGYY")

// this set of function is dedicated to "coloring the keyboard"
// for the keyboard, what we want is to get the "best" status among all the guesses
// if a letter was once green, it is green
// else if it was once yellow, yellow
// else if it was once red, red
// else we haven't tried it yet

// this function takes a list of guesses, for any letters,
// and returns the ones for the letter we are interested in
// it returns only the "kind" of the guess
// (since we know they are for the letter we are looking for already)
const allGuessesForLetter = (letter: Letter, allGuesses: WordGuess): Guess[] =>
  allGuesses.filter((g) => g.letter.character === letter.character).map((g) => g.kind)

// this function picks the "best guess" for this letter
// according to the "algorithm" on lines 195--198
const bestGuessForLetter = (letter: Letter, allGuesses: WordGuess): LetterGuess => {
  const letterGuesses = allGuessesForLetter(letter, allGuesses)
  if (letterGuesses.find((g) => g === "correct")) return {letter, kind: "correct"}
  if (letterGuesses.find((g) => g === "misplaced")) return {letter, kind: "misplaced"}
  if (letterGuesses.find((g) => g === "absent")) return {letter, kind: "absent"}
  // if we reach here, the letter has not been tried yet
  return {letter, kind: "untried"}
}

// this function takes a "list of letters" as a string, and returns the "best guess" for each of them
// notice that this function recomputes the guesses each time it is called
// this could be optimized and stored as state
// is this always necessary?
// if you store as state, you have to remember to update it after each new guess
// in this particular case, I decided it is ok to recompute the guesses multiple times,
// since this is not a lot of data (6 guesses of 5 letters)
const bestGuesses = (letters: string, guesses: string[], answer: string): WordGuess => {
  // we compute all the status of the guesses, for all guesses so far
  // and we "join them" together in a flat list (using flatMap instead of map)
  const allGuesses = guesses.flatMap((g) => rateGuesses(g, answer))
  return stringToWord(letters).map((letter) => bestGuessForLetter(letter, allGuesses))
}

type GameVariant = "wordle" | "dordle"
type GameMode = "easy" | "hard"
// this is a type definition for the overall state of the game
interface GameState {
  // the game variant (wordle or dordle)
  variant: GameVariant
  // the list of guesses so far
  guesses: string[]
  // the last of partially-inserted guess (not yet validated)
  candidateGuess: string
  // what the correct word is
  answers: string[]
  // the list of possible words that are answers (from the file)
  words: string[]
  // the list of all valid words (from the file)
  validwords: string[]
  // the maximum number of guesses allowed
  maxGuesses: number
  // the mode of the game
  mode: GameMode
  // the statistics that we accumulated
  statistics: Statistic[]
  // the status of the game
  status: Status
}
interface Statistic {
  variant: GameVariant
  answers: string[]
  guessesNo: number
}

// the higher-level game logic is here

// first, is a guess valid?
// in easy mode, it is valid if it belongs to a certain list of words
const isValid = (guess: string, words: string[]): boolean =>
  words.find((w) => w === guess) !== undefined

// in hard mode, it is valid if it is "similar enough" to the previous guess
// all the yellow or green characters are used again
const isValidHard = (guess: string, previous: string, answers: string[]): boolean => {
  // here we recompute a guess status (again!)
  // and extract the yellow and green letters
  const previousChars = rateGuesses(previous, answers[0])
    .filter((g) => g.kind !== "absent")
    .map((g) => g.letter.character)
  // we make sure all of them are in the current guess
  return previousChars.every((character) => guess.includes(character))
}

// some tests for this
//console.assert(isValidHard("abcde", "abcdf", ["abcde"]))
//console.assert(!isValidHard("zxvab", "abcdf", ["abczx"]))
//console.assert(isValidHard("zcvab", "abcdf", ["abczx"]))

// the function that tests if a guess is valid, taking into account the game mode
const isValidGuess = (guess: string, {mode, validwords, answers, guesses}: GameState) => {
  if (!isValid(guess, validwords)) {
    return false
  }
  // additional check if we are in hard mode
  if (mode === "hard" && guesses.length > 1) {
    return isValidHard(guess, guesses[guesses.length - 1], answers)
  }
  return true
}

// this section deals with multiple turns of the game

// the possible statuses are:
// - win: the game was won (the last guess is completely correct)
// - lost: the game was lost (guess number 6 is not completely correct)
// - next: the game is neither won nor lost, the player can continue
// additional statuse depending on player input
// - retry: the input was incorrect
// - new: the player wishes for a new game
// - invalid_hard: the guess is valid, but it didn't use all previously discovered letters
type Status = "win" | "lost" | "next" | "retry" | "new" | "invalid_hard"

// a function to determine the status of the game (except player input)
const status = ({guesses, answers, maxGuesses}: GameState): "win" | "lost" | "next" => {
  // we compare it to the answer, to know if we won
  if (answers.every((answer) => guesses.some((guess) => guess === answer))) return "win"
  // we count the number of guesses made, to know if we lost
  if (guesses.length === maxGuesses) return "lost"
  // otherwise, the game is unfinished
  return "next"
}

// this is a function to start a new game
// it takes the the existing game, and:
// - resets the guesses
// - picks a new word
// - if needed, it adds statistics
// we need the other data from the game, such as:
// - the game mode
// - the existing statistics

const maxGuessesForVariant = {
  wordle: 5,
  dordle: 7,
}

// generate a new game, starting from the finished one
const newGame = (prevGame: GameState): GameState => {
  const answers = getSolutions(
    prevGame.words,
    prevGame.variant === "dordle" ? ["random", "random"] : prevGame.answers,
  )
  const maxGuesses = maxGuessesForVariant[prevGame.variant]
  const gameStatus = status(prevGame)
  // we make a copy of the game, erasing the guesses, adding the answer
  // adding statistics, but ONLY if the game was finished
  const newGame = {...prevGame, guesses: [], answers, maxGuesses}

  switch (gameStatus) {
    case "next":
      return newGame // unfinished game, statistics not added
    case "lost":
      return {
        ...newGame,
        statistics: [
          ...prevGame.statistics,
          {
            variant: prevGame.variant,
            answers: prevGame.answers,
            guessesNo: prevGame.guesses.length + 1,
          },
        ],
        status: "new",
      }
    case "win":
      return {
        ...newGame,
        statistics: [
          ...prevGame.statistics,
          {
            variant: prevGame.variant,
            answers: prevGame.answers,
            guessesNo: prevGame.guesses.length,
          },
        ],
        status: "new",
      }
  }
}

// retrieve a random or precise solution
const getSolutions = (words: string[], indexes: string[]): string[] => {
  return indexes.map((index) => {
    let idx = Number(index)
    if (index === "random" || isNaN(idx) || idx >= words.length)
      idx = Math.floor(Math.random() * words.length)
    return words[idx]
  })
}

const processGuess = (game: GameState): GameState => {
  if (game.candidateGuess.length === 5 && isValidGuess(game.candidateGuess, game)) {
    // if the guess is valid, then we add the guess to the list of guesses
    const newGame = {...game, guesses: [...game.guesses, game.candidateGuess], candidateGuess: ""}
    // and we compute the game status, with the new guess (win, lost, next)
    return {...newGame, status: status(newGame)}
  } else if (
    game.mode === "hard" &&
    game.guesses.length > 1 &&
    !isValidHard(game.candidateGuess, game.guesses[game.guesses.length - 1], game.validwords)
  ) {
    // we are in hard mode, but the candidate guess didn't use all previously discovered letters
    return {...game, status: "invalid_hard"}
  } else {
    // otherwise, the guess was not valid, we try again
    return {...game, status: "retry"}
  }
}

type CssColors = "#0c8" | "#fc0" | "#f88" | "floralwhite"
const colors = {
  correct: "#0c8",
  misplaced: "#fc0",
  wrong: "#f88",
  untried: "floralwhite",
}

export {
  newGame,
  processGuess,
  maxGuessesForVariant,
  rateGuesses,
  bestGuesses,
  stringToWord,
  colors,
  Guess,
  GameState,
  Status,
  LetterGuess,
  Letter,
  Statistic,
  GameVariant,
  GameMode,
  WordGuess,
  CssColors,
}
