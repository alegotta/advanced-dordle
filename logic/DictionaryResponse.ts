//generated and adapted from https://app.quicktype.io/
export interface DictionaryResponse {
  word: string
  phonetics: any
  meanings: Meaning[]
  license: any
  sourceUrls: any
}

interface Meaning {
  partOfSpeech: string
  definitions: Definition[]
  synonyms: any
  antonyms: any
}

interface Definition {
  definition: string
  synonyms: any
  antonyms: any
  example?: string
}
