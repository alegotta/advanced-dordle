import {useEffect, useState} from "react"
import ErrorBoundary from "react-native-error-boundary"
import {GameState} from "./logic/logic"
import {initGame, newGame} from "./logic/uiLogic"
import {ErrorScreen} from "./screens/Error"
import {GameScreen} from "./screens/Game"
import {LoadingScreen} from "./screens/Loading"
import {SettingsScreen} from "./screens/Settings"

export default function App() {
  const [showGame, setShowGame] = useState<boolean>(false)
  const [gameState, setGameState] = useState<GameState | null>(null)

  useEffect(() => {
    initGame().then((gameState) => setGameState(gameState))
  }, [])

  return (
    <ErrorBoundary FallbackComponent={ErrorScreen}>
      {gameState === null ? (
        <LoadingScreen />
      ) : showGame ? (
        <GameScreen gameState={gameState} setGameState={setGameState} setShowGame={setShowGame} />
      ) : (
        <SettingsScreen
          gameState={gameState}
          newGame={(newState: GameState) => setGameState(newGame(newState))}
          setShowGame={setShowGame}
        />
      )}
    </ErrorBoundary>
  )
}
