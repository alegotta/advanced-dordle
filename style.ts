import {StatusBar, StyleSheet, Dimensions} from "react-native"
import {colors} from "./logic/logic"

const globals = {
  colors: {
    primary: "black",
    accent: "floralwhite",
    ...colors,
  },
  sizes: {
    normal: 15,
    title: 25,
  },
}
const styles = StyleSheet.create({
  fullwidth: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  container: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    gap: Math.round(globals.sizes.normal / 2),
    justifyContent: "center",
    marginTop: StatusBar.currentHeight,
    padding: globals.sizes.normal,
  },
  flexCol: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    alignContent: "center",
    justifyContent: "center",
    gap: Math.round(globals.sizes.normal / 2),
  },
  statistics: {
    borderColor: globals.colors.primary,
    borderWidth: 1,
  },
  flexRow: {
    display: "flex",
    flexDirection: "row",
    textAlign: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  header: {
    paddingBottom: globals.sizes.title,
  },
  letter: {
    alignItems: "center",
    borderWidth: Math.round(globals.sizes.normal / 7),
    display: "flex",
    fontSize: globals.sizes.normal,
    height: Math.round(globals.sizes.normal * 2),
    justifyContent: "center",
    margin: Math.round(globals.sizes.normal / 7),
    width: Math.round(globals.sizes.normal * 2),
  },
  lightText: {
    fontWeight: "300",
  },
  text: {
    fontSize: globals.sizes.normal,
    textTransform: "uppercase",
  },
  title: {
    fontSize: globals.sizes.title,
    fontWeight: "bold",
  },
  toggle: {
    alignItems: "center",
    backgroundColor: globals.colors.accent,
    color: globals.colors.primary,
    display: "flex",
    height: Math.round(globals.sizes.normal * 2.2),
    justifyContent: "center",
    textAlign: "center",
    textTransform: "uppercase",
    width: Math.round(globals.sizes.normal * 5.5),
  },
  toggleFocused: {
    backgroundColor: globals.colors.primary,
    color: globals.colors.accent,
    fontWeight: "400",
  },
  toggleMargins: {
    borderColor: globals.colors.primary,
    borderStyle: "solid",
    borderWidth: Math.round(globals.sizes.normal / 7),
    flexDirection: "row",
  },
})
export {styles}
