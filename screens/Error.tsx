import {UppercaseText} from "../components/UppercaseText"
import {BaseScreen} from "./BaseScreen"

const ErrorScreen = (props: {error: Error}) => (
  <BaseScreen name={"error ⚠️"}>
    <UppercaseText subtitle>{props.error.toString()}</UppercaseText>
    <UppercaseText>See the console for more information</UppercaseText>
  </BaseScreen>
)

export {ErrorScreen}
