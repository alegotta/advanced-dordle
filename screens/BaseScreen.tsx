import React from "react"
import {FlexStyle, View} from "react-native"
import {UppercaseText} from "../components/UppercaseText"
import {styles} from "../style"

const BaseScreen = ({
  name = "",
  inner = false,
  style,
  children,
}: {
  name?: string
  inner?: boolean
  style?: FlexStyle
  children?: React.ReactNode
}) => {
  return (
    <View style={[styles.container, style, !inner && styles.fullwidth]}>
      {name.length > 0 && (
        <UppercaseText h1={!inner} h3={inner}>
          {name}
        </UppercaseText>
      )}
      {children}
    </View>
  )
}
export {BaseScreen}
