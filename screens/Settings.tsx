import React from "react"
import {View} from "react-native"
import {BlackWhiteButton, BlackWhiteInput, SingleButton} from "../components/Buttons"
import {UppercaseText} from "../components/UppercaseText"
import {GameMode, GameState, GameVariant, Statistic} from "../logic/logic"
import {stats, vibrate} from "../logic/uiLogic"
import {styles} from "../style"
import {BaseScreen} from "./BaseScreen"

const SettingsScreen = ({
  gameState,
  newGame,
  setShowGame,
}: {
  gameState: GameState
  newGame: (gameState: GameState) => void
  setShowGame: (show: boolean) => void
}) => {
  return (
    <BaseScreen name="ordle">
      <SettingsToggle
        names={["dordle", "wordle"]}
        focused={gameState.variant}
        onValueChanged={(value: string) => newGame({...gameState, variant: value as GameVariant})}
      />
      {gameState.variant === "wordle" && (
        <SettingsToggle
          names={["easy", "hard"]}
          focused={gameState.mode}
          onValueChanged={(value: string) => newGame({...gameState, mode: value as GameMode})}
        />
      )}
      {gameState.variant === "wordle" && (
        <SettingsToggle
          names={["number", "random"]}
          focused={"random"}
          onValueChanged={(value: string) => newGame({...gameState, answers: [value]})}
        />
      )}
      <SingleButton
        title={"play"}
        onPress={() => {
          vibrate("win")
          setShowGame(true)
        }}
      />

      <StatisticSection
        gameState={gameState}
        resetStatistics={() => newGame({...gameState, statistics: []})}
      />
    </BaseScreen>
  )
}

const StatisticSection = ({
  gameState,
  resetStatistics,
}: {
  gameState: GameState
  resetStatistics: () => void
}) => {
  return (
    <BaseScreen inner name="statistics" style={styles.statistics}>
      {gameState.statistics.length === 0 ? (
        <UppercaseText>Nothing to show here! Play a game...</UppercaseText>
      ) : (
        <>
          <VariantStatistics statistics={gameState.statistics} variant="wordle" />
          <VariantStatistics statistics={gameState.statistics} variant="dordle" />
          <SingleButton title="reset" onPress={() => resetStatistics()} />
        </>
      )}
    </BaseScreen>
  )
}
const VariantStatistics = ({
  statistics,
  variant,
}: {
  statistics: Statistic[]
  variant: GameVariant
}) => {
  const strStats = stats(statistics, variant)
  return (
    <View style={styles.flexCol}>
      {strStats.length > 0 && <UppercaseText h3>{variant}</UppercaseText>}
      {strStats.map((stat) => (
        <UppercaseText key={stat}>{stat}</UppercaseText>
      ))}
    </View>
  )
}
const SettingsToggle = ({
  names,
  focused,
  onValueChanged,
}: {
  names: string[]
  focused: string
  onValueChanged: (value: string) => void
}) => {
  const [firstFocused, setFirstFocused] = React.useState(names[0] === focused)
  const toggleFirstFocused = () => setFirstFocused(!firstFocused)

  return (
    <View style={styles.toggleMargins}>
      {names[0] === "number" ? (
        <BlackWhiteInput
          focused={firstFocused}
          onFocus={() => toggleFirstFocused()}
          onValueChanged={(text: string) => onValueChanged(text)}
        />
      ) : (
        <BlackWhiteButton
          text={names[0]}
          focused={firstFocused}
          onValueChanged={(text: string) => {
            toggleFirstFocused()
            onValueChanged(text)
          }}
        />
      )}
      <BlackWhiteButton
        text={names[1]}
        focused={!firstFocused}
        onValueChanged={(text: string) => {
          toggleFirstFocused()
          onValueChanged(text)
        }}
      />
    </View>
  )
}
export {SettingsScreen}
