import {MaterialIcons} from "@expo/vector-icons"
import {LinearGradient} from "expo-linear-gradient"
import React from "react"
import {Pressable, Share, View} from "react-native"
import {SingleButton} from "../components/Buttons"
import {UppercaseText} from "../components/UppercaseText"
import {DictionaryHint, getDictionaryHint} from "../logic/getFromApi"
import {CssColors, GameState, Letter} from "../logic/logic"
import {
  formatGuesses,
  formatKeyboard,
  getGameSchema,
  getStatusMessage,
  letterPress,
  vibrate,
  WordGuessWithColors,
} from "../logic/uiLogic"
import {styles} from "../style"
import {BaseScreen} from "./BaseScreen"

const GameScreen = ({
  gameState,
  setGameState,
  setShowGame,
}: {
  gameState: GameState
  setGameState: (g: GameState) => void
  setShowGame: (val: boolean) => void
}) => {
  const onPress = (letter: string) => setGameState(letterPress(letter, gameState))

  return (
    <BaseScreen name="game">
      <View style={styles.flexRow}>
        <Board gameState={gameState} index={0} />
        {gameState.variant === "dordle" && <Board gameState={gameState} index={1} />}
      </View>
      <Keyboard keyboard={formatKeyboard(gameState)} onPress={onPress} />
      <UppercaseText>{getStatusMessage(gameState.status)}</UppercaseText>
      <SingleButton onPress={() => setShowGame(false)} title={"settings"} />
    </BaseScreen>
  )
}

const Board = ({gameState, index}: {gameState: GameState; index: number}) => {
  return (
    <View>
      {formatGuesses(gameState, index).map((guess, index) => (
        <BoardWord word={guess} wordIndex={index} key={index} />
      ))}
      <GameStatusSection gameState={gameState} index={index} />
    </View>
  )
}

const GameStatusSection = ({gameState, index}: {gameState: GameState; index: number}) => {
  const [action, setAction] = React.useState<DictionaryHint>("None")
  const status = gameState.status
  vibrate(status)

  console.log(gameState.answers[0])
  return (
    <BaseScreen inner>
      <View style={styles.flexRow}>
        {status === "next" && <SingleButton title="Clue" onPress={() => setAction("Type")} />}
        {status === "next" && gameState.maxGuesses - gameState.guesses.length === 1 && (
          <SingleButton title="Desperate Clue" onPress={() => setAction("Example")} />
        )}
        {status === "win" && (
          <SingleButton title="Definition" onPress={() => setAction("Definition")} />
        )}
        {status === "win" && (
          <MaterialIcons
            name="share"
            color="black"
            size={24}
            onPress={() => void Share.share({message: getGameSchema(gameState)})}
          />
        )}
      </View>
      <DictionaryText word={gameState.answers[index]} action={action} />
    </BaseScreen>
  )
}

const DictionaryText = ({word, action}: {word: string; action: DictionaryHint}) => {
  const [value, setValue] = React.useState<string>("")

  React.useEffect(() => {
    const exec = async () => {
      if (action !== "None") {
        const {data} = await getDictionaryHint(word, action)
        setValue(`${action}: ${data}`)
      }
    }
    void exec()
  }, [word, action])

  return <UppercaseText>{value}</UppercaseText>
}

const BoardWord = ({word, wordIndex}: {word: WordGuessWithColors; wordIndex: number}) => {
  return (
    <View style={styles.flexRow}>
      {word.map(({letter, colors}, index) => (
        <LetterComponent key={`${wordIndex}-${index}`} letter={letter} colors={colors} />
      ))}
    </View>
  )
}

const Keyboard = ({
  keyboard,
  onPress,
}: {
  keyboard: WordGuessWithColors
  onPress: (letter: string) => void
}) => {
  const slice = <Type,>(arr: Type[], perChunk: number): Type[][] => {
    return arr.reduce((all: Type[][], one, i: number) => {
      const ch = Math.floor(i / perChunk)
      all[ch] = [].concat(all[ch] || [], one)
      return all
    }, [])
  }

  return (
    <View>
      {slice(keyboard, 10).map((row, i) => (
        <KeyboardRow row={row} rowIndex={i} onPress={onPress} key={i} />
      ))}
    </View>
  )
}

const KeyboardRow = ({
  row,
  rowIndex,
  onPress = () => null,
}: {
  row: WordGuessWithColors
  rowIndex: number
  onPress: (letter: string) => void
}) => {
  return (
    <View style={styles.flexRow}>
      {row.map(({letter, colors}, index) => (
        <LetterComponent
          key={`${rowIndex}-${index}`}
          letter={letter}
          colors={colors}
          onPress={onPress}
        />
      ))}
    </View>
  )
}

const LetterComponent = ({
  letter,
  colors,
  onPress = () => null,
}: {
  letter: Letter
  colors: CssColors[]
  onPress?: (letter: string) => void
}) => {
  return (
    <Pressable onPress={() => onPress(letter.character)}>
      <LinearGradient
        colors={colors}
        locations={[0.5, 0.5]}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={styles.letter}
      >
        <UppercaseText h3>{letter.character}</UppercaseText>
      </LinearGradient>
    </Pressable>
  )
}
export {GameScreen}
