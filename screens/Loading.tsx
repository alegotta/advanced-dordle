import {BaseScreen} from "./BaseScreen"

const LoadingScreen = () => {
  return <BaseScreen name="Loading" />
}

export {LoadingScreen}
